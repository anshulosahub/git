## 1. Server

### 1.1 Create repository (server/ cloud)
login into your account and create a new repository.
For example I have just created one repo name "c"

>note: do not opt for readme file. i.e. your repo should be blank

## 2. Local

* open terminal or command prompt
* go to the directory where you have your code
* run below commands

### 2.1 initialize git

```
git init
```

### 2.2 tell git who you are

```
git config user.email "anshul@osahub.com"
git config  user.name "Anshul Jain"
```

### 2.3 add origin
for this copy url from browser (here `c` is the name of the repository)

```
git remote add origin https://bitbucket.org/anshulosahub/c
```

if you get any errors, then you may also try below command

```
git remote remove origin //to unset origin

git remote add origin https://anshulosahub@bitbucket.org/anshulosahub/c.git
```

### 2.4 stage all changes

```
git add .
```

### 2.5 commit staged changes

```
git commit -m "initial commit"
```

### 2.6 push committed changes

```
git push origin master
```