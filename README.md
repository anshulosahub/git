## What is this repository for? 

* to help people with git commands

### How do I set up git? 

* https://help.github.com/articles/set-up-git/

### Who do I talk to? 

* anshul@osahub.com

### References

* https://dont-be-afraid-to-commit.readthedocs.io/en/latest/git/commandlinegit.html