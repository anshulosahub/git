## 1. Server

### 1.1 Create repository (server/ cloud)
login into your account and create a new repository.
For example I have just created one repo name "python"

>note: do not opt for readme file. i.e. your repo should be blank

## 2. Local

* open terminal or command prompt
* go to the directory where you clone your repository
* run below commands

### 2.1 Clone empty git repo

```
git clone https://anshulosahub@bitbucket.org/anshulosahub/python.git
```

> above URL may vary for you, to be on safer side copy url from browser

go into the newly created local git repo

```
cd python
```

### 2.2 tell git who you are

```
git config user.email "anshul@osahub.com"
git config  user.name "Anshul Jain"
```

### 2.3 Create the develop branch with README file

```
git checkout -b develop
touch README.md
touch CHANGELOG.md
```

Add content to README.md file and save the changes

Sample changelog.md

```
# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

## [0.0.0] - 2017-03-17
### Added
- Initial setup

[0.0.0]: https://bitbucket.org/anshulosahub/newangular9ui/commits/tag/0.0.0
```


### 2.4 Create and push initial commit with tag 0.0.0

```
git status
git add .
git commit -m "Initial commit"
git tag -a 0.0.0 -m "Version 0.0.0"
git push origin develop --tags
```

### 2.5 Create master branch

```
git checkout -b master
git push origin master
```