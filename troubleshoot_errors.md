# Common errors #

## 403 ##

### on windows ###

#### try updating git using below commands  

```
git --version //to get current version
git update-git-for-windows //to update if your current git version is > 2.16.1(2)
git update //for version between 2.14.2 and 2.16.1
```

#### login using OAuth  

After it update when you try to access any git repo, login popup comes,
there you can login using credentials or just below the login button,
there is an option to login using OAuth, use that.
