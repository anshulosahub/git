## initialize git in any directory
```
git init
```

## tell git who you are 
### globally (default for all repositories)
```
git config --global user.email "anshul@osahub.com"
git config --global user.name "Anshul Jain"
```

### locally (for current repository only)
```
git config user.email "anshul@osahub.com"
git config  user.name "Anshul Jain"
```

## add origin 
for this copy url from browser
```
git remote add origin https://bitbucket.org/anshulosahub/git
```

## to check untracked changes or to check file which has been modified
```
git status
```

## to check/ see changes made in file
```
git diff file1.txt
```

## to revert changes of any particular file
```
git checkout -- file1.txt
```

## to revert all local changes (unstaged)
```
git reset --hard HEAD
```

>Note: resets your changes back to the **last commit** that your **local repo** has tracked. If you made a commit, did not push it to GitHub, that will not be removed.

## to stage all changes
```
git add .
```

## to stage specific changes
```
git add file1.txt
```
or 
```
git add *.txt
```

## to unstage
```
git rm --cached file1.txt
```

## to delete any file
```
git rm file1.txt
```

## commit staged changes
```
git commit -m "commit message"
```

## push committed changes
```
git push origin master
```

## to see the commit history
- You can use any of below mentioned command to see the commit history.
- Most recent commit is displayed on top.
- Use 'Down' and 'Up' arrow keys to browse through the commit history
- press 'q' to exit from commit history view

### detailed view
```
git log
```

### compact view
```
git log --oneline
```

## create branch locally
```
git checkout -b my-new-branch
```

## use a branch which already exists
```
git checkout my-new-branch
```

## delete branch locally
```
git branch -d my-new-branch
```

## Switching between branches locally
```
git checkout branch_name
```

## pushing existing branch to another repo
```
git remote add origin https://anshulosahub@bitbucket.org/anshulosahub/git.git
git push -u origin master
```


## Making changes to commit
We can modify which we have already made (to both local and remote repository)
We have be cautious while doing this and sure about what we are doing and why

### Change order of commits
After switching to that branch use following commands
```
<<ToDo
```

### Change commit message of latest/ most recent commit (reword)
After switching to that branch use following commands
```
<<ToDo
```
### Change commit message of any older commit (reword)
After switching to that branch use following commands
```
<<ToDo
```

### Merge/ group multiple commits into a single commit (squash)
- This will merge code of all commits into most recent one
- All commit mesages will be preserved
- User is also given an option to modify the commit message
```
<<ToDo
```

### Merge/ group multiple commits into a single commit (ignoring the commit mesage) (fixup)
- This will merge code of all commits into most recent one
- Only most recent commit mesages will be preserved
- User is not given an option to modify the commit message
```
<<ToDo
```