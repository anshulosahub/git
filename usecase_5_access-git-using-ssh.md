There are mainly two ways to access git

* using http/ https
* ssh 

# Need & Benefit

* If we use git using commandline then we may need to enter username and password everytime we are running any command which needs to talk to our cloud/remote repository.
* Using ssh, after setting required keys, we can execute command withot any need to enter username and password everytime.
* It become very easy/convinient to access git from production server and CI CD tools like Jenkins

_**Note:**_ be careful while using this if yo are using multiple git accounts (eg. personal and official) on same machine.

# Steps

## 1. Generate an SSH key

Open terminal to create ssh keys:

```
cd ~                 #Your home directory
ssh-keygen -t rsa    #Press enter for all values
```

provide other information like name (eg. anshul-rsa-key) of key and password, if you want to encrypt your key (to avoid misuse)

### load your SSH (private) key

```
ssh-add ~/Documents/anshul-rsa-key
```

### Check if your SSH key is loaded

```
ssh-add -l
```

### For Windows

(Only works if the commit program is capable of using certificates/private & public ssh keys)
Use Putty Gen to generate a key
Export the key as an open SSH key

Here is a [walkthrough](http://ask-leo.com/how_do_i_create_and_use_public_keys_with_ssh.html) on putty gen for the above steps

## 2. Associate the SSH key with the remote repository

This step varies, depending on how your remote is set up.

* If it is a GitHub repository and you have administrative privileges, go to settings and click 'add SSH key'. Copy the contents of your ~/.ssh/id_rsa.pub into the field labeled 'Key'.
* If your repository is administered by somebody else, give the administrator your id_rsa.pub.
* If your remote repository is administered by your, you can use this command for example:

```
scp ~/.ssh/id_rsa.pub YOUR_USER@YOUR_IP:~/.ssh/authorized_keys/id_rsa.pub
```

## 3. Set your remote URL to a form that supports SSH 1

If you have done the steps above and are still getting the password prompt, make sure your repo URL is in the form

```
git+ssh://git@github.com/username/reponame.git
```

as opposed to

```
https://github.com/username/reponame.git
```

To see your repo URL, run:

```
git remote show origin
```

You can change the URL with:

```
git remote set-url origin git+ssh://git@github.com/username/reponame.git
```

> Note: steps copied from [this link](https://stackoverflow.com/questions/8588768/how-do-i-avoid-the-specification-of-the-username-and-password-at-every-git-push)