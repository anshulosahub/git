## 1. Create a local repository in the temp-dir directory using: ##

```
git clone <url to ORI repo> temp-dir
```

clone ori repository

## 2. Go into the temp-dir directory. ##

## 3. To see a list of the different branches in ORI do: ##

```
git branch -a
git branch-a
```

## 4. Checkout all the branches that you want to copy from ORI to NEW using: ##

```
git checkout branch-name
```

checkout-branches

## 5. Now fetch all the tags from ORI using: ##

```
git fetch --tags
git-fetch-tags
```

## 6. Before doing the next step make sure to check your local tags and branches using the following commands: ##

```
git tag
git branch -a
```

## 7. Now clear the link to the ORI repository with the following command: ##

```
git remote rm origin
```

## 8. Now link your local repository to your newly created NEW repository using the following command: ##

```
git remote add origin <url to NEW repo>
```

## 9. Now push all your branches and tags with these commands: ##

```
git push origin --all
git push --tags
```

Now you have a full copy from your ORI repo.

> Note:

```
git clone --mirror <url to ORI repo> temp-dir
```